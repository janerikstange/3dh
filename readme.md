
# Usage
Run
```
npm install
yarn
```
to install all required node.js packages

Run
```
npm run dev
yarn dev
```
to build a development version and serve it from [http://localhost:8080/](http://localhost:8080/)

Run
```
npm run build
yarn build
```
to build a deployment version (into the build directory)

